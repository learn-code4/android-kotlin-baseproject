package com.f1k375.basecode

import android.app.Application
import com.f1k375.basecode.data.remote.configs.Network
import com.f1k375.basecode.data.remote.configs.RemoteModule
import com.f1k375.basecode.presentation.modules.PresentationModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        //initiation for using koin, required
        startKoin {
            androidContext(applicationContext)
            modules(
                listOf(
                    RemoteModule.networkModule(),
                    RemoteModule.serviceModule(),
                    RemoteModule.persistenceModule(),
                    RemoteModule.repositoryModule(),
                    PresentationModules.useCaseModule(),
                    PresentationModules.viewModelModule()
                )
            )
        }
    }

}
