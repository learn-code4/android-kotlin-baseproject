package com.f1k375.basecode.commons.extentions

/*
 * collection of android extension
 *
 */

import android.animation.TimeInterpolator
import android.animation.ValueAnimator
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Point
import android.os.Build
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import com.f1k375.basecode.R
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout

/**
 * Show snack bar with action or on dismiss method
 *
 * this method can only be used in activity
 * you can custom your desire snackbar here for this project
 */
fun Activity.showSnackBarWithAction(
    view: View,
    message: String,
    actionText: String = "Coba lagi",
    action: () -> Unit,
    actionOnDismiss: () -> Unit = {}
) {
    val snackBar = Snackbar.make(
        view, message,
        Snackbar.LENGTH_LONG
    ).addCallback(object : Snackbar.Callback() {
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            super.onDismissed(transientBottomBar, event)
            if (event != DISMISS_EVENT_ACTION)
                actionOnDismiss()
        }
    }).setAction(actionText) {
        action()
    }
//    snackBar.setActionTextColor(
//        ResourcesCompat.getColor(
//            resources,
//            R.color.design_default_color_secondary, theme
//        )
//    )
    val snackBarView = snackBar.view
    snackBarView.setBackgroundColor(Color.BLACK)
    val textView =
        snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
    textView.setTextColor(Color.WHITE)
    snackBar.show()
}

/**
 * Show snack bar
 * this snackbar does not contain callback for action
 *
 * this method can only be used in activity
 * you can custom your desire snackbar here for this project
 */
fun Activity.showSnackBar(
    view: View,
    message: String,
    onDismiss: () -> Unit = {}
) {
    val snackBar = Snackbar.make(
        view, message,
        Snackbar.LENGTH_LONG
    ).addCallback(object : Snackbar.Callback() {
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            super.onDismissed(transientBottomBar, event)
            onDismiss()
        }
    })

    val snackBarView = snackBar.view

    snackBarView.setBackgroundColor(Color.BLACK)
    val textView =
        snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
    textView.setTextColor(Color.WHITE)
    snackBar.show()
}

/**
 * Show snack bar without callback
 *
 * this method can only be used in activity
 * you can custom your desire snackbar here for this project
 */
fun Fragment.showSnackBar(
    view: View,
    message: String,
    onDismiss: () -> Unit = {}
) {
    val snackBar = Snackbar.make(
        view, message,
        Snackbar.LENGTH_LONG
    ).addCallback(object : Snackbar.Callback() {
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            super.onDismissed(transientBottomBar, event)
            onDismiss()
        }
    })

    val snackBarView = snackBar.view
    snackBarView.setBackgroundColor(Color.BLACK)
    val textView =
        snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
    textView.setTextColor(Color.WHITE)
    snackBar.show()
}

/**
 * Show snack bar success or error message
 *
 * this method can only be used in Fragment
 * position on top of screen
 * you can custom your desire snackbar here for this project
 */

fun showSnackbarTop(message: String, view: View, isError: Boolean = false) {
    val color =
        if (isError)
            view.context.getColor(R.color.red_50)
        else
            view.context.getColor(R.color.green_50)
    val textColor =
        if (isError)
            view.context.getColor(R.color.red_500)
        else
            view.context.getColor(R.color.green_500)
    val colorBlack = view.context.getColor(R.color.black_60)

    val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        .setBackgroundTint(color)
        .setTextColor(textColor)
        .setActionTextColor(colorBlack)
        .setAction("OK") {
        }

    val snackView = snackbar.view
    val layoutParam = FrameLayout.LayoutParams(
        FrameLayout.LayoutParams.MATCH_PARENT,
        FrameLayout.LayoutParams.WRAP_CONTENT,
    )
    layoutParam.gravity = Gravity.TOP
    layoutParam.topMargin = 24
    snackView.layoutParams = layoutParam

    snackbar.show()
}


/**
 * get Screen width
 */
inline val Context.screenWidth: Int
    get() = Point().also {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            (getSystemService(Context.WINDOW_SERVICE) as WindowManager).currentWindowMetrics.bounds.width()
        } else
            (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.getSize(it)
    }.x

inline val View.screenWidth: Int
    get() = context!!.screenWidth

/**
 * convert Int to Dp
 */
inline val Int.dp: Int
    get() = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), Resources.getSystem().displayMetrics
    ).toInt()

/**
 * convert Float to Dp
 */
inline val Float.dp: Float
    get() = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, this, Resources.getSystem().displayMetrics
    )


inline fun getValueAnimator(
    forward: Boolean = true,
    duration: Long,
    interpolator: TimeInterpolator,
    crossinline updateListener: (progress: Float) -> Unit
): ValueAnimator {
    val a =
        if (forward) ValueAnimator.ofFloat(0f, 1f)
        else ValueAnimator.ofFloat(1f, 0f)
    a.addUpdateListener { updateListener(it.animatedValue as Float) }
    a.duration = duration
    a.interpolator = interpolator
    return a
}


fun blendColors(color1: Int, color2: Int, ratio: Float): Int {
    val inverseRatio = 1f - ratio

    val a = (Color.alpha(color1) * inverseRatio) + (Color.alpha(color2) * ratio)
    val r = (Color.red(color1) * inverseRatio) + (Color.red(color2) * ratio)
    val g = (Color.green(color1) * inverseRatio) + (Color.green(color2) * ratio)
    val b = (Color.blue(color1) * inverseRatio) + (Color.blue(color2) * ratio)
    return Color.argb(a.toInt(), r.toInt(), g.toInt(), b.toInt())
}

/**
 * Get color resource from fragment
 * return color resource of the given Color Resource Id
 */
fun Fragment.getColorResource(color: Int): Int {
    return ResourcesCompat.getColor(resources, color, null)
}

/**
 * from fragment
 * Create color state list from the given color resource id
 */
fun Fragment.createColorStateList(color: Int): ColorStateList {
    return ColorStateList.valueOf(getColorResource(color))
}

/**
 * Get color resource from activity
 * return color resource of the given Color Resource Id
 */
fun Activity.getColorResource(color: Int): Int {
    return ResourcesCompat.getColor(resources, color, null)
}

/**
 * from fragment
 * Create color state list from the given color resource id
 */
fun Activity.createColorStateList(color: Int): ColorStateList {
    return ColorStateList.valueOf(getColorResource(color))
}

/**
 * Used for hiding keyboard, from fragment
 *
 * @see Activity.hideKeyboard
 */
fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

/**
 * used for copy text
 */
fun Activity.copyText(text: String) {
    val copyManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clipData = ClipData.newPlainText("key", text)
    copyManager.setPrimaryClip(clipData)
    Toast.makeText(this, "berhasil disalin", Toast.LENGTH_SHORT).show()
}

/**
 * used for copy text
 */
fun Fragment.copyText(text: String) {
    val copyManager =
        requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clipData = ClipData.newPlainText("key", text)
    copyManager.setPrimaryClip(clipData)
    Toast.makeText(requireContext(), "berhasil disalin", Toast.LENGTH_SHORT).show()
}

/**
 * Used for hiding keyboard
 *
 * @see Context.hideKeyboard
 */
fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

/**
 * Used for hiding keyboard
 */
fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Activity.showToast(message: String, isLongDisplay: Boolean = false) {
    Toast.makeText(
        applicationContext, message,
        if (isLongDisplay)
            Toast.LENGTH_LONG
        else
            Toast.LENGTH_SHORT
    ).show()
}

fun Fragment.showToast(message: String, isLongDisplay: Boolean = false) {
    Toast.makeText(
        requireContext().applicationContext, message,
        if (isLongDisplay)
            Toast.LENGTH_LONG
        else
            Toast.LENGTH_SHORT
    ).show()
}

fun TextInputLayout.removeErrorStateOnTextChange(editText: EditText) {
    editText.doAfterTextChanged {
        isErrorEnabled.doThis {
            isErrorEnabled = false
        }
    }
}

fun TextInputLayout.alertError(message: String) {
    isErrorEnabled = true
    error = message
}


fun EditText.value(): String {
    return this.text.trim().toString()
}

fun View.show() {
    this.isVisible = true
}

fun View.hide() {
    this.isVisible = false
}

fun View.invisible() {
    this.isInvisible = true
}
