package com.f1k375.basecode.commons.extentions
import java.util.*

/**
 * As name initial
 *
 * take name in format like AA, MM, AL
 */
fun String.asNameInitial(): String {
    val nameSplit = this.split(" ")
    return if (nameSplit.size > 1)
        nameSplit.firstOrNull()?.firstOrNull()?.uppercase() +
                nameSplit.lastOrNull()?.firstOrNull()?.uppercase()
    else
        nameSplit.firstOrNull()?.firstOrNull()?.uppercase() +
                nameSplit.firstOrNull()?.firstOrNull()?.uppercase()
}

fun Int.makePinIndexes(): String {
    return IntArray(this) { Random().nextInt(7 -1) + 1}.asList().joinToString("")
}

fun String.pinIndexToList(): List<Int> {
    return this.map { it.digitToInt() }.toList()
}

/**
 * Convert to color hex format
 *
 * Use it with Color.parseColor
 */
fun String.convertToColorHex(): String {
    var hash = 0
    this.forEach {
        hash = it.hashCode() + ((hash.shl(5)) - hash)
        hash = hash.and(hash)
    }

    var color = "#"
    for (i in 0..2) {
        val value = hash shr i * 8 and 255
        color += ("00" + value.toString(16)).takeLast(2)
    }
    return color
}

/**
 * Do this
 *
 * only running the function when true
 */
fun Boolean.doThis(job: () -> Unit) {
    if (this)
        job()
}

/**
 * Do this
 *
 * only running the function when true
 * and return it value
 */
fun Boolean.alsoDoThis(job: () -> Unit): Boolean {
    if (this)
        job()
    return this
}

fun Boolean.alsoNotDoThis(job: () -> Unit): Boolean {
    if (!this)
        job()
    return this
}

/**
 * Do this
 *
 * not running the function when true
 */
fun Boolean.notDoThis(job: () -> Unit) {
    if (!this)
        job()
}

/**
 * Split ignore empty
 *
 * return list for split that avoid [] result
 *
 * [] -> in not empty list, that list contain empty string
 */
fun CharSequence.splitIgnoreEmpty(vararg delimiters: String): List<String> {
    return this.split(*delimiters).filter {
        it.isNotEmpty()
    }
}

fun String.getProperNameDisplay(limitChar: Int = 15): String {
    val countSpace = splitIgnoreEmpty(" ").size
    val name = (if (countSpace > 2)
        substring(0, indexOf(" ", indexOfFirst { it == ' ' } + 1))
    else
        this
            )

    return if (name.length > limitChar) name.replace(" ", "\n")
    else name
}

/**
 * To map
 *
 * return map with <String,String> format
 *
 * example string format
 * {key1=value1, key2=value2}
 *
 * supported delimiter '=', ':', and ';'
 */
fun String.toMap(): Map<String, String> {
    fun getDelimiter(string: String): String {
        return when {
            string.contains("=") -> "="
            string.contains(":") -> ":"
            else -> ";"
        }
    }

    return if (startsWith("{") && endsWith("}")) {
        val data = replace(" ", "").removeSurrounding("{", "}").splitIgnoreEmpty(",")
        val map = mutableMapOf<String, String>()

        data.forEach {
            val keyPairValue = it.splitIgnoreEmpty(getDelimiter(it))
            (keyPairValue.size > 1).doThis {
                map[keyPairValue[0]] = keyPairValue[1]
            }
        }
        map
    } else
        emptyMap()
}
