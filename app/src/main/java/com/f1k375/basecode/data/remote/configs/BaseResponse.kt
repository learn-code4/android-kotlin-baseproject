package com.f1k375.basecode.data.remote.configs

import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

// please change depend on response you get for base, remember this work only if response inside in data key
// take a look for response in here https://api.instantwebtools.net/v1/passenger?page=1&size=10

// if json key for each response is difference please don't use this
// for example response like from https://dummyjson.com/productsthis


/**
 * Base response
 *
 * {
 *  ok : false,
 *  message : "some message",
 *  data : [] or {}, // any data like post or list of posts, product etc.
 *  current_page: 1,
 *  last_page: 5,
 *  total: 100,
 *  errors : {} //some error info given from server
 *
 * }
 */
data class BaseResponse<T>(
    @SerializedName("ok")
    @Expose
    val status: Boolean? = false,

    @SerializedName("message")
    @Expose
    val message: String? = null,

    @SerializedName("data")
    @Expose
    val data: T? = null,

    @SerializedName("current_page")
    @Expose
    val currentPage: Int? = null,

    @SerializedName("last_page")
    @Expose
    val lastPage: Int? = null,

    @SerializedName("total")
    @Expose
    val total: Int? = null,

    @SerializedName("errors")
    @Expose
    val errors: JsonObject? = null,

    var errorCode: Int = 0
)
