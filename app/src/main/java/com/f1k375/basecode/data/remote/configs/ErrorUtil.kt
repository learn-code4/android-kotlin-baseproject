package com.f1k375.basecode.data.remote.configs

import com.google.gson.Gson
import com.google.gson.JsonNull.INSTANCE
import com.google.gson.JsonParser

/**
 * Error util
 *
 * for handle response request error body
 *
 * please modify for your requirement
 */
object ErrorUtil{

    fun errorMessage(response: String?): BaseResponse<Nothing>? {
        val fileJsonResponse = try {
            JsonParser.parseString(response)
        }catch (e: Exception){
            INSTANCE
        }
        return Gson().fromJson(fileJsonResponse, BaseResponse<Nothing>()::class.java)
    }

}
