package com.f1k375.basecode.data.remote.configs

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.f1k375.basecode.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Network {

    const val BASE_URL = "https://example.com/" // todo change this
    const val API_V1 = "api/v1/"

    /** example using coin not inside activity
     *
     * private val loginHelper by KoinJavaComponent.inject<AccountHelper>(AccountHelper::class.java)
     *
     * */

    fun getRetrofit(okhttpClient: OkHttpClient, baseUrl: String): Retrofit {
        val gson = GsonBuilder()
            .setLenient()
            .create()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okhttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    // for listen network view other app, show notification
    fun getChucker(context: Context) =
        ChuckerInterceptor.Builder(context)
            .collector(ChuckerCollector(context))
            .maxContentLength(250000L)
            .redactHeaders(emptySet())
            .alwaysReadResponseBody(false)
            .build()

    fun getOkhttp(chuckerInterceptor: ChuckerInterceptor): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()

        if (BuildConfig.IS_RELEASE.not()) {
            // for listen network view console
            val httpLoggingInterceptor = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)
            okHttpClientBuilder.addInterceptor(chuckerInterceptor)
        }

        val connectionSpec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
            .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_3)
            .cipherSuites(
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
            ).build()

        okHttpClientBuilder.connectionSpecs(listOf(connectionSpec))

        okHttpClientBuilder.connectTimeout(20, TimeUnit.SECONDS)
        okHttpClientBuilder.readTimeout(20, TimeUnit.SECONDS)
        okHttpClientBuilder.writeTimeout(20, TimeUnit.SECONDS)

        okHttpClientBuilder.addInterceptor { chain ->
            val mOriRequest: Request = chain.request()
//            val token = loginHelper.tokenAuth
            val token = "YOUR_TOKEN_HERE" //change this with your token, or pass from shared preference or other resource
            val request: Request = mOriRequest.newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .addHeader("Accept", "application/json")
                .method(mOriRequest.method, mOriRequest.body)
                .build()
            chain.proceed(request)
        }

        return okHttpClientBuilder.build()
    }
}
