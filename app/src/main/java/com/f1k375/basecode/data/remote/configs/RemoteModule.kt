package com.f1k375.basecode.data.remote.configs

import com.f1k375.basecode.data.remote.example.service.ExamplePersistence
import com.f1k375.basecode.data.remote.example.service.ExamplePersistenceImpl
import com.f1k375.basecode.data.remote.example.service.ExampleService
import com.f1k375.basecode.data.repositories.example.ExampleRepository
import com.f1k375.basecode.data.repositories.example.ExampleRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

object RemoteModule {

    fun networkModule() = module {
        // single mean, each call just create one instance an return that instance without creating new
        // please read koin docs about this
        single { Network.getChucker(androidContext()) }
        single { Network.getOkhttp(get()) }

        single { Network.getRetrofit(get(), Network.BASE_URL) }
        // how about a have multiple base url for retrofit, well you can use named in here
        // single(named("otherRetrofit")) { Network.getRetrofit(get(), "SOME_OTHER_BASE_URL") }
    }

    fun serviceModule() = module {
        single<ExampleService> { get<Retrofit>().create(ExampleService::class.java) }

        // you can get retrofit instance with specific base url using named, make sure value named is match
        // single<ExampleService> { get<Retrofit>(named("otherRetrofit")).create(ExampleService::class.java) }
    }

    fun persistenceModule() = module {
        single<ExamplePersistence> { ExamplePersistenceImpl(get()) }
    }

    fun repositoryModule() = module {
        single<ExampleRepository> { ExampleRepositoryImpl(get()) }
    }
}
