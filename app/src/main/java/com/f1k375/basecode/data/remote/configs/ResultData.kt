package com.f1k375.basecode.data.remote.configs

import com.f1k375.basecode.domain.ErrorDomain
import com.f1k375.basecode.domain.asErrorDomain
import retrofit2.HttpException

/**
 * Result data
 *
 * @param T
 * @constructor Create empty Result data
 *
 * please modify as you like,
 */
sealed class ResultData<out T: Any>{
    data class Success<out T: Any>(val data: T): ResultData<T>()
    data class Failed(private val _errorResponse: BaseResponse<Nothing>? = null, val exception: Exception? = null): ResultData<Nothing>(){
        val errorResponse = (_errorResponse?.asErrorDomain()?: ErrorDomain(false,"",null,0)).apply {
            if (exception is HttpException) errorCode = exception.code()
        }
    }

    override fun toString(): String {
        return when(this){
            is Success<*> -> """
                success with data : $data
            """.trimIndent()
            is Failed -> """
                failed, response: $errorResponse
                may caused by ${exception?.localizedMessage}
            """.trimIndent()
        }
    }
}

/**
 * `true` if [ResultData] is of type [Success] & holds non-null [Success.data].
 */
val ResultData<*>.succeeded
    get() = this is ResultData.Success && data != null
