package com.f1k375.basecode.data.remote.example.payload.repuest

import com.google.gson.annotations.SerializedName

data class ExampleRequest(

	@SerializedName("password")
	val password: String? = null,

	@SerializedName("username")
	val username: String? = null
)
