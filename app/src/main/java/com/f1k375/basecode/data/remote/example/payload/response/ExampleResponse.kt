package com.f1k375.basecode.data.remote.example.payload.response

import com.google.gson.annotations.SerializedName

data class ExampleResponse(
	@SerializedName("data")
	val data: String? = null
)
