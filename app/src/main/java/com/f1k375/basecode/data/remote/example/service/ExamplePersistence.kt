package com.f1k375.basecode.data.remote.example.service

import com.f1k375.basecode.data.remote.configs.BaseResponse
import com.f1k375.basecode.data.remote.configs.ResultData
import com.f1k375.basecode.data.remote.example.payload.response.ExampleResponse

interface ExamplePersistence {

    // in case without pagination
    suspend fun getExamples(payload: Int): ResultData<List<ExampleResponse>>

    // in case response with pagination
    suspend fun getExamplesWithPagination(payload: Int): ResultData<BaseResponse<List<ExampleResponse>>>

    suspend fun getExampleDetail(payload: Int): ResultData<ExampleResponse>

}
