package com.f1k375.basecode.data.remote.example.service

import com.f1k375.basecode.data.remote.configs.*
import com.f1k375.basecode.data.remote.example.payload.response.ExampleResponse

class ExamplePersistenceImpl(
    private val service: ExampleService
): ExamplePersistence {

    override suspend fun getExamples(payload: Int): ResultData<List<ExampleResponse>> =
        safeApiCall(
            call = { responseHandler(service.getExamples(payload)) },
            errorMessage = "Failed to get example/nCrash on system"
        )

    override suspend fun getExamplesWithPagination(payload: Int): ResultData<BaseResponse<List<ExampleResponse>>> =
        safeApiCall(
            call = { responsePagingHandler(service.getExamples(payload)) },
            errorMessage = "Failed to get example/nCrash on system"
        )

    override suspend fun getExampleDetail(payload: Int): ResultData<ExampleResponse> =
        safeApiCall(
            call = { responseHandler(service.getExampleDetail(payload)) },
            errorMessage = "Failed to get example detail/nCrash on system"
        )
}
