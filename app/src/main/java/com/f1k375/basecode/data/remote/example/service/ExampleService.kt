package com.f1k375.basecode.data.remote.example.service

import com.f1k375.basecode.data.remote.configs.BaseResponse
import com.f1k375.basecode.data.remote.configs.Network.API_V1
import com.f1k375.basecode.data.remote.example.payload.response.ExampleResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val EXAMPLE = "$API_V1/example"

interface ExampleService {

    @GET("$EXAMPLE/list")
    suspend fun getExamples(
        @Query("page") page: Int
    ): Response<BaseResponse<List<ExampleResponse>>>

    @GET("$EXAMPLE/show/{id}")
    suspend fun getExampleDetail(
        @Path("id") id: Int
    ): Response<BaseResponse<ExampleResponse>>

}
