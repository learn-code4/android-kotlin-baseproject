package com.f1k375.basecode.data.repositories.example

import com.f1k375.basecode.data.remote.configs.ResultData
import com.f1k375.basecode.domain.PaginationDomain
import com.f1k375.basecode.domain.example.ExampleDomain

interface ExampleRepository {

    // in case without pagination
    suspend fun getExamples(payload: Int): ResultData<List<ExampleDomain>>

    // in case response with pagination
    suspend fun getExamplesWithPagination(payload: Int): ResultData<PaginationDomain>

    suspend fun getExampleDetail(payload: Int): ResultData<ExampleDomain>

}
