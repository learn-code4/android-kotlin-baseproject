package com.f1k375.basecode.data.repositories.example

import com.f1k375.basecode.data.remote.configs.ResultData
import com.f1k375.basecode.data.remote.configs.succeeded
import com.f1k375.basecode.data.remote.example.service.ExamplePersistence
import com.f1k375.basecode.domain.PaginationDomain
import com.f1k375.basecode.domain.asPaginationDomain
import com.f1k375.basecode.domain.example.ExampleDomain
import com.f1k375.basecode.domain.example.asDomain

/**
 * Example repository impl
 *
 * @property persistence
 * @constructor Create empty Example repository impl
 *
 *
 * in this class you handle both from server or local data
 */
class ExampleRepositoryImpl(
    private val persistence: ExamplePersistence
): ExampleRepository {
    override suspend fun getExamples(payload: Int): ResultData<List<ExampleDomain>> {
        val resultRemote = persistence.getExamples(payload) // response from server
        // val resultLocal = exampleDao.getExamples() // response from local
        // manage result that you will be returned from this method

        return if (resultRemote.succeeded){
            resultRemote as ResultData.Success
            ResultData.Success(
                resultRemote.data.asSequence().map{ it.asDomain }.toList()
            )
        }else{
            resultRemote as ResultData.Failed
        }
    }

    override suspend fun getExamplesWithPagination(payload: Int): ResultData<PaginationDomain> {
        val resultRemote = persistence.getExamplesWithPagination(payload)
        return if (resultRemote.succeeded){
            resultRemote as ResultData.Success
            val dataDomain = resultRemote.data.data?.asSequence()?.map{ it.asDomain }?.toList() ?: emptyList()
            ResultData.Success(resultRemote.data.asPaginationDomain().apply { data = dataDomain })
        }else{
            resultRemote as ResultData.Failed
        }
    }

    override suspend fun getExampleDetail(payload: Int): ResultData<ExampleDomain> {
        val resultRemote = persistence.getExampleDetail(payload)
        return if (resultRemote.succeeded){
            resultRemote as ResultData.Success
            ResultData.Success(
                resultRemote.data.asDomain
            )
        }else{
            resultRemote as ResultData.Failed
        }
    }

}
