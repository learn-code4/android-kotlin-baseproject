package com.f1k375.basecode.domain

import com.f1k375.basecode.data.remote.configs.BaseResponse
import com.google.gson.JsonObject

data class ErrorDomain(
    val status: Boolean,
    val message: String,
    val errors: JsonObject?,
    var errorCode: Int = 0,
)

fun <T> BaseResponse<T>.asErrorDomain() =
    ErrorDomain(
        status?:false,
        message?:"",
        errors,
        errorCode
    )
