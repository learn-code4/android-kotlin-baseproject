package com.f1k375.basecode.domain

import com.f1k375.basecode.data.remote.configs.BaseResponse


data class PaginationDomain(
    val status: Boolean,
    val message: String,
    var data: Any?,
    val currentPage: Int,
    val lastPage: Int,
    val total: Int,
    val isLastPage: Boolean = currentPage >= lastPage
)

fun <T> BaseResponse<T>.asPaginationDomain() =
    PaginationDomain(
        status ?: false,
        message ?: "",
        data = data,
        currentPage ?: 0,
        lastPage ?: 0,
        total ?: 0
    )
