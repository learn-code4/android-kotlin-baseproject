package com.f1k375.basecode.domain.example

import android.os.Parcelable
import com.f1k375.basecode.data.remote.example.payload.response.ExampleResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class ExampleDomain(
    val data: String
): Parcelable

val ExampleResponse.asDomain
get() = ExampleDomain(
    data = data ?: ""
)
