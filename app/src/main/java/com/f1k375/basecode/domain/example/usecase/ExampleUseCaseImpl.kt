package com.f1k375.basecode.domain.example.usecase

import com.f1k375.basecode.data.remote.configs.ResultData
import com.f1k375.basecode.data.repositories.example.ExampleRepository
import com.f1k375.basecode.domain.PaginationDomain
import com.f1k375.basecode.domain.example.ExampleDomain

/**
 * Example use case impl
 *
 * @property repository
 * @constructor Create empty Example use case impl
 *
 *
 * what is this class for? just return same like repo does
 *
 * well, this is optional you can use this or just skip using repo
 * this used when you manage call from repo, like calling multiple repo for just one step of feature
 * any logical step for use case in your feature do it here
 */
class ExampleUseCaseImpl(
    private val repository: ExampleRepository
): ExampleUseCase {
    override suspend fun getExamples(payload: Int): ResultData<List<ExampleDomain>> = repository.getExamples(payload)

    override suspend fun getExamplesWithPagination(payload: Int): ResultData<PaginationDomain> = repository.getExamplesWithPagination(payload)

    override suspend fun getExampleDetail(payload: Int): ResultData<ExampleDomain> = repository.getExampleDetail(payload)
}
