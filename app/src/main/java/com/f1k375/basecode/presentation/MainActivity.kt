package com.f1k375.basecode.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.f1k375.basecode.databinding.ActivityMainBinding
import com.f1k375.basecode.presentation.utils.viewmodel.ViewState
import com.f1k375.basecode.presentation.utils.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)

    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        getData()
        setObserver()
    }

    private fun getData() {
//        viewModel.getExamples()
//        viewModel.resetListExample()
//        viewModel.getExampleDetail(1)
    }

    private fun setObserver(){
        setListExample()
        setExampleDetailObserver()
    }

    private fun setListExample(){
        viewModel.listExample.observe(this){result->
            when(result){
                is ViewState.Loading -> {
                    // todo loading
                }
                is ViewState.Error -> {
                    // todo handle error
                }
                is ViewState.DataLoaded -> {
                    // todo success get data
                }
            }
        }
    }

    private fun setExampleDetailObserver(){
        viewModel.exampleDetail.observe(this){result->
            when(result){
                is ViewState.Loading -> {
                    // todo loading
                }
                is ViewState.Error -> {
                    // todo handle error
                }
                is ViewState.DataLoaded -> {
                    // todo success get data
                }
            }
        }
    }
}
