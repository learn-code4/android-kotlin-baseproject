package com.f1k375.basecode.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.f1k375.basecode.commons.extentions.alsoNotDoThis
import com.f1k375.basecode.commons.extentions.notDoThis
import com.f1k375.basecode.domain.example.ExampleDomain
import com.f1k375.basecode.domain.example.usecase.ExampleUseCase
import com.f1k375.basecode.presentation.utils.viewmodel.BaseViewModel
import com.f1k375.basecode.presentation.utils.viewmodel.ViewState

class MainViewModel(
    private val useCase: ExampleUseCase
): BaseViewModel() {

    private var currentPage = 1
    private var isLastPage = false

    fun loadMoreExample(){
        isLastPage.notDoThis {
            getExamples()
        }
    }

    fun resetListExample(){
        currentPage = 1
        isLastPage = false
        getExamplesWithPagination()
    }

    private val _listExample = MutableLiveData<ViewState<List<ExampleDomain>>>()
    val listExample: LiveData<ViewState<List<ExampleDomain>>>
    get() = _listExample

    fun getExamples(){
        callRequest(
            liveData = _listExample,
            call = {useCase.getExamples(currentPage)}
        )
    }

    private fun getExamplesWithPagination(){
        callRequestPagination<ExampleDomain>(
            call = {useCase.getExamplesWithPagination(currentPage)},
            success = {data, isLast->
                isLastPage = isLast.alsoNotDoThis {
                    currentPage++
                }
                _listExample.postValue(ViewState.DataLoaded(data))
            }, failed = {message, error ->
                _listExample.postValue(ViewState.Error(message, error))
            }
        )
    }

    private val _exampleDetail = MutableLiveData<ViewState<ExampleDomain>>()
    val exampleDetail: LiveData<ViewState<ExampleDomain>>
    get() = _exampleDetail

    fun getExampleDetail(id: Int){
        callRequest(
            liveData = _exampleDetail,
            call = {useCase.getExampleDetail(id)},
        )
    }


}
