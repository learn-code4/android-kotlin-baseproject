package com.f1k375.basecode.presentation.modules

import com.f1k375.basecode.domain.example.usecase.ExampleUseCase
import com.f1k375.basecode.domain.example.usecase.ExampleUseCaseImpl
import com.f1k375.basecode.presentation.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object PresentationModules {

    fun useCaseModule() = module {
        // factory mean, each call is creating new instance
        factory<ExampleUseCase> { ExampleUseCaseImpl(get()) }
    }

    fun viewModelModule() = module {
        viewModel { MainViewModel(get()) }
    }

}
