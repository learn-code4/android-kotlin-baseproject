package com.f1k375.basecode.presentation.utils.adapter

class AdapterListener {
    interface OnclickItem {
        fun onItemCLick(data: Any?, position: Int)
    }

    interface OnLastItem {
        fun onLoadMore()
    }
}
