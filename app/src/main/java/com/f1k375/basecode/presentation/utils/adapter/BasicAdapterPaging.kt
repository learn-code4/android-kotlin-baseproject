package com.f1k375.basecode.presentation.utils.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BasicAdapterPaging<DATA, VIEW : RecyclerView.ViewHolder> :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val ITEM_TYPE = 0
        private const val LOADING_TYPE = 1
    }

    abstract fun viewHolderItem(parent: ViewGroup): VIEW
    abstract fun holderItem(holder: VIEW, position: Int, item: DATA)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == LOADING_TYPE)
            ViewHolderLoadingAdapter.initiation(parent)
        else viewHolderItem(parent)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder !is ViewHolderLoadingAdapter)
            holderItem(holder as VIEW, position, data[position]!!)

        if (data.lastIndex == position && isLoading.not() && data[position] != null) {
            lastItem?.onLoadMore()
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position] == null)
            LOADING_TYPE
        else ITEM_TYPE
    }

    protected var clickItem: AdapterListener.OnclickItem? = null
    fun onItemClick(listener: AdapterListener.OnclickItem) {
        clickItem = listener
    }

    private var lastItem: AdapterListener.OnLastItem? = null
    fun onLastItem(listener: AdapterListener.OnLastItem) {
        lastItem = listener
    }

    val adapterData: List<DATA>
        get() = data.filterNotNull()

    protected val data = ArrayList<DATA?>()
    protected var isLoading = false

    fun onLoadNewData() {
        isLoading = true
        data.add(null)
        notifyItemInserted(data.lastIndex)
    }

    fun onDoneLoad() {
        if (data.contains(null)) {
            data.remove(null)
            notifyItemRemoved(data.size)
        }
    }

    fun addData(newData: List<DATA>) {
        onDoneLoad()

        val lastPosition = data.size
        data.addAll(newData)
        notifyItemRangeInserted(lastPosition, newData.size)
    }

    protected fun updateData(indexUpdate: Int, updatedData: DATA) {
        if (indexUpdate >= 0) {
            data[indexUpdate] = updatedData
            notifyItemChanged(indexUpdate)
        }
    }

    protected fun removeData(indexDeleted: Int) {
        if (indexDeleted >= 0) {
            data.removeAt(indexDeleted)
            notifyItemRemoved(indexDeleted)
        }
    }

    fun clearData() {
        val temp = data.size
        data.clear()
        notifyItemRangeRemoved(0, temp)
    }

}
