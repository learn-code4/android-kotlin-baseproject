package com.f1k375.basecode.presentation.utils.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.f1k375.basecode.databinding.ItemLoadingAdapterBinding

class ViewHolderLoadingAdapter(@NonNull private val mBinding: ItemLoadingAdapterBinding): RecyclerView.ViewHolder(mBinding.root){

    companion object{
        fun initiation(parent: ViewGroup) = ViewHolderLoadingAdapter(
            ItemLoadingAdapterBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

}
