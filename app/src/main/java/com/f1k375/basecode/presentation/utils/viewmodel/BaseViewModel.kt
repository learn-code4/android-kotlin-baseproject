package com.f1k375.basecode.presentation.utils.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.f1k375.basecode.data.remote.configs.ResultData
import com.f1k375.basecode.data.remote.configs.succeeded
import com.f1k375.basecode.domain.PaginationDomain
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

open class BaseViewModel : ViewModel() {

    protected fun <T : Any> callRequest(
        liveData: MutableLiveData<ViewState<T>>? = null,
        liveDataOneTime: MutableLiveData<OneTimeEvent<ViewState<T>>>? = null,
        call: suspend () -> ResultData<T>,
        success: (T) -> Unit = {},
        failed: (message: String, error: JsonObject?) -> Unit = { _, _ -> }
    ) {
        liveData?.postValue(ViewState.Loading)
        liveDataOneTime?.postValue(ViewState.Loading.toOneTimeEvent())

        viewModelScope.launch {
            val result = call()
            if (result.succeeded) {
                result as ResultData.Success

                success(result.data)

                liveData?.postValue(ViewState.DataLoaded(result.data))
                liveDataOneTime?.postValue(ViewState.DataLoaded(result.data).toOneTimeEvent())
            } else {
                result as ResultData.Failed
                val errorCode = result.errorResponse.errorCode

                val message = StringBuilder()
                message.append(with(result.errorResponse.message) {
                    when {
                        isEmpty() -> result.exception?.message ?: "Unknown Error"
                        errorCode == 401 && result.errorResponse.message.contains(
                            "Username",
                            ignoreCase = true
                        ).not() -> "Authentication Failed\nSilakan login ulang"
                        else -> "Code : ${result.errorResponse.errorCode}, ${result.errorResponse.message}"
                    }
                })

                val errors = result.errorResponse.errors?.also { error ->
                    error.keySet().toList().forEach {
                        error.getAsJsonArray(it).firstOrNull()?.asString?.let { note ->
                            message.append("\n$note")
                        }
                    }
                }


                failed(message.toString(), errors)
                Log.e("view-model", "error $result")
                liveData?.postValue(ViewState.Error(message.toString(), errors, errorCode))
                liveDataOneTime?.postValue(
                    ViewState.Error(message.toString(), errors, errorCode).toOneTimeEvent()
                )
            }
        }
    }

    protected fun <T : Any> callRequestPagination(
        call: suspend () -> ResultData<PaginationDomain>,
        success: (List<T>, isLast: Boolean) -> Unit = { _, _ -> },
        failed: (message: String, error: JsonObject?) -> Unit = { _, _ -> }
    ) {
        viewModelScope.launch {
            val result = call()
            if (result.succeeded) {
                result as ResultData.Success
                val data = (result.data.data ?: emptyList<T>()) as List<T>
                success(data, result.data.isLastPage)
            } else {
                result as ResultData.Failed
                val message = StringBuilder()

                message.append(with(result.errorResponse.message) {
                    if (isEmpty())
                        result.exception?.message ?: "Unknown Error"
                    else "Code : ${result.errorResponse.errorCode}, ${result.errorResponse.message}"
                })

                val errors = result.errorResponse.errors?.also { error ->
                    error.keySet().toList().forEach {
                        error.getAsJsonArray(it).firstOrNull()?.asString?.let { note ->
                            message.append("\n$note")
                        }
                    }
                }
                failed(message.toString(), errors)
            }
        }
    }

}
