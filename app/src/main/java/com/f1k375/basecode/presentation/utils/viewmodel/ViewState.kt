package com.f1k375.basecode.presentation.utils.viewmodel

import com.google.gson.JsonObject

sealed class ViewState<out T: Any> {
    data class DataLoaded<out T: Any>(val data: T) : ViewState<T>()
    object Loading : ViewState<Nothing>()
    data class Error(val message: String, val errors: JsonObject? = null, val errorCode: Int? = null) : ViewState<Nothing>()
}
